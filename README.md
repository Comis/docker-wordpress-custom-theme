# Docker Compose w/ WordPress mySQL phpMyAdmin

Simple base setup of `docker-compose` with `WordPress`, `mySQL`, `phpMyAdmin`
and example `theme`.

## Using

To run `docker-compose up -d` (without stdout/logs), remove `-d` to see stdout
in terminal.

If running with `-d` you need to stop it with `docker-compose down`.

### Resetting

By default mySQL data is not removed on each `docker-compose up`, but you can
reset this database by running: `docker-compose down --volumes`.

## Services

### WordPress

Vanilla setup. See source docker image for more configuration options.

More themes can be placed in `themes/` folder. Currently there is
`twentyseventeen` which is automatically used.

I have edited `themes/twentyseventeen/footer.php` just a little bit while
testing.

Note: `theme` folder is bound to the running instance, so you can have a quick
development cycle.

### mySQL

Vanilla setup. See source docker image for more configuration options.

### phpMyAdmin

Vanilla setup. See source docker image for more configuration options.
