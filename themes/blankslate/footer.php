</div>
<footer id="footer" role="contentinfo">
  <div class="copyright">
    <?php echo sprintf(__('%1$s %2$s %3$s. All Rights Reserved.', 'blankslate'), '&copy;', date('Y'), esc_html(get_bloginfo('name')));
echo sprintf(__(' Theme By: %1$s.', 'Bjarte'), 'Bjarte'); ?>
  </div>
</footer>
</div>
<?php wp_footer();?>
</body>

</html>