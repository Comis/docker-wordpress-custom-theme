<!DOCTYPE html>
<html <?php language_attributes();?>>

<head>
  <meta charset="<?php bloginfo('charset');?>" />
  <meta name="viewport" content="width=device-width" />
  <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>" />
  <?php wp_head();?>
  <script type="text/javascript" src="http://livejs.com/live.js">
  </script>
</head>

<body <?php body_class();?>>
  <div id="wrapper" class="hfeed">
    <div id="header" role="banner" class="header">
      <div id="branding" class="header__branding">
        <div id="site-title" class="header__branding__title<?php if (is_front_page() || is_home()) {echo ' header__branding__title-frontpage';}?>">
          <a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_html(get_bloginfo('name')); ?>" rel="home">
            <?php echo esc_html(get_bloginfo('name')); ?>
          </a>
        </div>
        <div id="site-description" class="header__branding__description">
          <?php bloginfo('description');?>
        </div>
      </div>
      <div class="header__pages" id="menu" role="navigation">
        <?php wp_nav_menu(array('theme_location' => 'main-menu'));?>
      </div>
    </div>
    <div class="content">